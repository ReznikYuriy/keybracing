import { addClass, createElement, removeClass } from "./helper.mjs";

const username = sessionStorage.getItem("username");

if (!username) {
  window.location.replace("/login");
}

const socket = io("", { query: { username } });

const disconnectBtn = document.getElementById("disconnectBtn");
const readyBtn = document.getElementById("readyBtn");
const timer = document.getElementById("timer");
const quoteDisplayElement = document.getElementById('quoteDisplay');
const quoteInputElement = document.getElementById('quoteInput');

let readyBtnStatus = false;

socket.on("USER_EXIST", () => {
  sessionStorage.clear();
  alert("User with such name already exist!");
  window.location.replace("/login");
});

socket.on("PLAYER_JOIN", (data) => {
  const users = data.users;
  drawUsers(users);
});

socket.on("USER_DISCONNECT", (data) => {
  const users = data.users;
  drawUsers(users);
});

socket.on("SERVER_UPDATED", (data) => {
  const users = data.users;
  drawUsers(users);
});

socket.on("GET_READY_TO_START", (data) => {
  const timerBeforeGame = data.timerBeforeGame;
  //console.log("TIMER BEFORE", timerBeforeGame);
  addClass(disconnectBtn, "hidden");
  addClass(readyBtn, "hidden");
  countDown(timerBeforeGame, timer);
});

socket.on("START_GAME", (data) => {
  console.log("START_GAME");
  const timerForGame = data.timerForGame;
  //console.log(timerForGame);
  countDown(timerForGame, timer);
  drawQuote(data.text);
  quoteInputElement.addEventListener('input', inputEventListener);
});

const drawQuote=(quote)=> {
  quoteDisplayElement.innerHTML = '';
  quote.split('').forEach(character => {
    const characterSpan = document.createElement('span')
    characterSpan.innerText = character
    quoteDisplayElement.appendChild(characterSpan)
  })
  quoteInputElement.value = null;
}
const drawUsers = (users) => {
  const sidebar = document.getElementById("sidebar");
  sidebar.innerHTML = "";
  users.forEach((user) => {
    const barContainer = createElement({
      tagName: "div",
      className: "racing_user-indicator",
    });
    const readyIndicator = createElement({
      tagName: "span",
      className: "indicator",
    });
    const userName = createElement({ tagName: "span", className: "user-name" });
    const indicator = createElement({
      tagName: "div",
      className: "progress-indicator",
    });
    const bar = createElement({ tagName: "div", className: "progress-bar" });
    user.isReady
      ? (addClass(readyIndicator, "user-ready"),
        removeClass(readyIndicator, "user-dont-ready"))
      : (addClass(readyIndicator, "user-dont-ready"),
        removeClass(readyIndicator, "user-ready"));
    userName.innerText = user.name;
    if (user.name === username) userName.innerText = user.name + " (you)";
    indicator.append(bar);
    barContainer.append(readyIndicator, userName, indicator);
    sidebar.append(barContainer);
    /* leftIndicator.style.width = `${firstFighter.health / maxHealthFirstFigter * 100}%`; */
  });
};
const onclickDisconnectBtn = () => {
  socket.emit("disconnect");
  sessionStorage.clear();
  window.location.replace("/login");
};
const onclickReadyBtn = () => {
  readyBtn.textContent = readyBtnStatus ? "READY" : "DONT READY";
  readyBtnStatus = !readyBtnStatus;
  socket.emit("CHANGE_READY_STATUS");
};

const inputEventListener=() => {
  const arrayQuote = quoteDisplayElement.querySelectorAll('span')
  const arrayValue = quoteInputElement.value.split('')

  let correct = true
  arrayQuote.forEach((characterSpan, index) => {
    const character = arrayValue[index]
    if (character == null) {
      removeClass(characterSpan, 'correct');
      removeClass(characterSpan, 'incorrect');
      correct = false
    } else if (character === characterSpan.innerText) {
      addClass(characterSpan, 'correct');
      removeClass(characterSpan,'incorrect');
      console.log("correct input");
      socket.emit("CORRECT_INPUT");
    } else {
      addClass(characterSpan, 'incorrect');
      removeClass(characterSpan,'correct');
      correct = false
    }
  });

  if (correct) socket.emit("USER_FINISH");
};

const countDown = (sec, element) => {
  let current = sec;

  let timerId = setInterval(function () {
    element.textContent = current;
    if (current === 0) {
      clearInterval(timerId);
    }
    current--;
  }, 1000);
};
disconnectBtn.addEventListener("click", onclickDisconnectBtn);
readyBtn.addEventListener("click", onclickReadyBtn);
