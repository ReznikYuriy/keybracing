export const isUserExistByName = (arr, username) => {
  for (let i = 0; i < arr.length; i++) {
    if (arr[i].name === username) {
      return true;
    }
  }
  return false;
};

export const findIndexByName = (arr, name) => {
  let len = arr.length;
  while (len--) {
    if (arr[len].name === name) {
      return len;
    }
  }
  return -1;
};

export const findIndexById = (arr, id) => {
  let len = arr.length;
  while (len--) {
    if (arr[len].id === id) {
      return len;
    }
  }
  return -1;
};
