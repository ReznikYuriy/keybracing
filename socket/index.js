import * as config from "./config";
import * as helper from "./helper";
import { texts } from "../data";

const users = [];
const textNumber = Math.floor(Math.random() * Math.floor(texts.length));
console.log("num", textNumber);

export default (io) => {
  io.on("connection", (socket) => {
    const username = socket.handshake.query.username;
    let currentUser = {
      id: socket.id,
      name: username,
      isReady: false,
      pointer: 0,
    };
    if (helper.isUserExistByName(users, currentUser.name)) {
      socket.emit("USER_EXIST");
      socket.disconnect();
    } else {
      if (currentUser.name !== "null") {
        users.push(currentUser);
        io.emit("PLAYER_JOIN", {
          users,
        });
      }
    }

    socket.on("disconnect", () => {
      if (helper.isUserExistByName(users, currentUser.name))
        users.splice(helper.findIndexByName(users, currentUser.name), 1);
      socket.broadcast.emit("USER_DISCONNECT", { id: currentUser.id, users });
      checkUsersStatus(users);
    });

    socket.on("CHANGE_READY_STATUS", () => {
      const userindex = helper.findIndexById(users, socket.id);
      users[userindex].isReady = !users[userindex].isReady;
      io.emit("SERVER_UPDATED", { users });
      checkUsersStatus(users);
    });

    socket.on("CORRECT_INPUT", () => {
      const userindex = helper.findIndexById(users, socket.id);
      users[userindex].pointer += 1;
      io.emit("SERVER_UPDATED", { users });
      console.log("correct input", users[userindex].pointer);
    });
  });
  const checkUsersStatus = (users) => {
    for (let i = 0; i <= users.length; i++) {
      if (users[i] === undefined) continue;
      if (users[i].isReady === false) return;
    }
    io.emit("GET_READY_TO_START", {
      timerBeforeGame: config.SECONDS_TIMER_BEFORE_START_GAME,
    });
    setTimeout(() => {
      console.log(texts[textNumber]);
      io.emit("START_GAME", {
        timerForGame: config.SECONDS_FOR_GAME,
        text: texts[textNumber],
      });
    }, config.SECONDS_TIMER_BEFORE_START_GAME * 1000);
  };
};
